# hello-node-docker

git clone https://gitlab.com/luciano.carranza/hello-node-docker.git

# Test locally
npm install
node server.js

# Build Dockerfile
docker build -t lucianocarranza/hello-node-docker:2.0.0 .

# Push image to DockerHub
docker push lucianocarranza/hello-node-docker:2.0.0

# Run docker image locally
docker run -p 7000:8080 lucianocarranza/hello-node-docker:2.0.0

# Add Heroku config

heroku login
# heroku create | heroku git:remote -a luchosite
heroku container:login
# heroku logs --tail
# git remote rm heroku
heroku container:push web
heroku container:release web
# heroku open

