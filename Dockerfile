# Use official node image from docker hub. Alpine version for smaller size
FROM node:12-alpine

# Set /app as the working directory
WORKDIR /app

# Copy files from local folder to /app container folder
COPY . .

# Install dependencies
RUN npm install

# Expose port 8080 in the container and run the web app
EXPOSE 8080
CMD [ "node", "server.js" ] 